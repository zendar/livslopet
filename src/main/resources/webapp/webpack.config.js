var MiniCssExtractPlugin  = require('mini-css-extract-plugin');

var plugins = [
    new MiniCssExtractPlugin ('bundle.css'), // <=== where should content be piped
];

const path = require('path');

module.exports = {
    watch: true,
    mode: "development", // "production" | "development" | "none",
	entry:  './src',
	output: {
	    path: path.resolve(__dirname, 'builds'), //__dirname + '/builds',
	    filename: 'bundle.js',
        publicPath: 'builds/',
	},
    plugins: plugins,
    module: {
        rules: [
            {
                test:   /\.js/,
                loader: 'babel',
                include: __dirname + '/src',
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                  // Creates `style` nodes from JS strings
                  'style-loader',
                  // Translates CSS into CommonJS
                  'css-loader',
                  // Compiles Sass to CSS
                  'sass-loader',
                ],
            },
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                  plugins: ['@babel/transform-runtime'],
                  presets: ['@babel/preset-env','@babel/react']//, '@babel/stage-1']
                }
            },
            {
                test: /\.css$/,
                use: [
                  // Creates `style` nodes from JS strings
                  'style-loader',
                    MiniCssExtractPlugin.loader, 
                    'css-loader'
                ],
            },
            {
                test:   /\.html/,
                loader: 'html',
            },
            {
                test: /\.png$/,
                loader: "url-loader?limit=100000"
            },
            {
                test: /\.jpg$/,
                loader: "file-loader"
            },
            {
                test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?limit=10000&mimetype=application/font-woff'
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?limit=10000&mimetype=application/octet-stream'
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file-loader'
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?limit=10000&mimetype=image/svg+xml'
            }
      ],
    }
};
