
import React from 'react';
import ReactDOM from 'react-dom';

import * as mobx from 'mobx';
import * as mobxReact from 'mobx-react';

const {observable, computed,autorun} = mobx;
const {observer} = mobxReact;

@observer
class HappyTracker extends React.Component {

  render() {

    var imgsrc = '';
      if (this.props.data.happymeter > 5) {
      imgsrc = "/images/boy-love.jpg";
      } else if (this.props.data.happymeter >= 0) {
      imgsrc = "/images/boy-smile.jpg";
      } else {
      imgsrc = "/images/boy-sad.jpg";
      }


    return (
      <div className="happyTracker">
        <small>Lykk-o-meter: {this.props.data.happymeter.toFixed(2)}</small>
        <p><img src={imgsrc} width='200' /></p>
      </div>
    );
  }
}


ReactDOM.render(
  <HappyTracker data={gameStore.data}/>,
  document.getElementById('happymeter')
);







