 
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import * as mobx from 'mobx';
import * as mobxReact from 'mobx-react';

const {observable, computed,autorun} = mobx;
const {observer} = mobxReact;

@observer
class AssetsTracker extends Component {

  render() {
    return (
      <span className="assetsTracker">
        {this.props.data.assets.toFixed(2)}
      </span>
    );
  }
}


ReactDOM.render(
  <AssetsTracker data={gameStore.data}/>,
  document.getElementById('assets')
);







