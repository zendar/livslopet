import $ from 'jquery';
import './gamificationBling.jsx';


/******************
***  The logic  ***
******************/

var stoppit = true;
var eventList = [];

window.doTick = function() {
            updateExpenses();
            updateSaving();
            updateNetWorth();
            updateLoans();
            updateEvents();

            // Update turn
            $('#turn').html(gameStore.data.turn);

            gameStore.data.turn++;
}


$(document).ready(function(){ /*code here*/


    console.log("STARTUP");

    var i = 12;

    setupEvents();

    // Set name
    $('#pname').html(gameStore.data.playerName);


    (function theLoop (i) {
        setTimeout(function () {
            doTick();
            // DO SOMETHING WITH data AND stuff
            if (--i && !stoppit) {                  // If i > 0, keep going
                theLoop(i);  // Call the loop again

                if (gameStore.data.turn % 10 == 0) {
//                        showWonderMessage('Completed turn nr: '+gameStore.data.turn);
                }


            }
        }, 100);
    })(i);

});



function updateSaving() {

    var totalSavedThisMonth = 0;

    for (var account of gameStore.data.savings) {

        var key = "savingtypes";
        var inter =  gameStore.data[key];
        inter = inter[account.type];
        inter = inter.interest;

//        console.log("account.principle for account ["+account.type+"]: "+account.principle+", inter: "+inter+", key = "+key);

        account.principle = account.principle + account.savePerMonth + account.principle * inter / 100 / 12;

        totalSavedThisMonth += account.principle;
    }


    gameStore.data.savingTotal = totalSavedThisMonth;
//    console.log("savingTotal: "+totalSavedThisMonth);
}

function updateLoans() {
    var account = gameStore.data.saving;

    var currentloans =0;

    for (var loan of gameStore.data.loans) {

            if (loan.current > 0) {
              loan.current = loan.current + (loan.current * loan.interest / 100 / 12) - loan.payment;
              if (loan.current <=0) {
                loan.current = 0;
                showWonderMessage('Lån \''+loan.itemName+'\' nedbetalt!');
                gameStore.data.happymeter +=5;
              }
            }

//        console.log('loan :'+loan.principle);
        currentloans += loan.current;
    }

    gameStore.data.debt = currentloans;

    gameStore.data.chartData.datasets[0].data[0] = currentloans;


    // Update debug label..
//    $('#loan').html(result);
}


function updateNetWorth() {
    var oldNetworth = gameStore.data.networth;
    var networth = 0;
    networth+= gameStore.data.savingTotal;

    for (var loan of gameStore.data.loans) {
//        console.log('loan :'+loan.principle);
        networth-= loan.current;
    }
    if (oldNetworth < 0 && networth > 0) {
        showWonderMessage('Du har positiv formue! Fortsett slik!');
        gameStore.data.happymeter +=10;
//        stoppit = true;
    }


    gameStore.data.networth = networth;

//    console.log('networth= '+networth);
}

function updateExpenses() {
    var expenses = 0;

    // Add monthly expenses
    for (var expense of gameStore.data.monthlyBills) {
//        console.log('loan :'+loan.principle);
        expenses+= expense.cost;
    }

    // Add loan payments
    for (var loan of gameStore.data.loans) {

            if (loan.current > 0) {
                expenses += loan.payment;
            }
    }

    // Add savings
    for (var account of gameStore.data.savings) {

        expenses += account.savePerMonth;
    }


    gameStore.data.expenses = expenses;

//    console.log('networth= '+networth);
}


function setupEvents() {

    for (var ev of events.data.badstuffs) {
        eventList.push(ev);
    }

}

function updateEvents() {

    if (eventList.length > 0 && gameStore.data.turn % 10 == 0) {
        var ev = eventList.pop();

//        showBadEventMessage(ev.title,ev.description);
        showModal(ev.title, ev.description, ev.moneydelta);
        stoppit = true;


        console.log("Event -> "+ev.description);
    }


}
