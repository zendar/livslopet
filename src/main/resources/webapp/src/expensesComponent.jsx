
import React from 'react';
import ReactDOM from 'react-dom';

import * as mobx from 'mobx';
import * as mobxReact from 'mobx-react';

const {observable, computed,autorun} = mobx;
const {observer} = mobxReact;

@observer
class ExpensesTracker extends React.Component {

  render() {
    return (
      <span className="expensesTracker">
        {this.props.data.expenses.toFixed(2)}
      </span>
    );
  }
}


ReactDOM.render(
  <ExpensesTracker data={gameStore.data}/>,
  document.getElementById('expenses')
);







