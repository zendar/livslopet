
import React from 'react';
import ReactDOM from 'react-dom';

import * as mobx from 'mobx';
import * as mobxReact from 'mobx-react';

const {observable, computed,autorun} = mobx;
const {observer} = mobxReact;

@observer
class MoneyTracker extends React.Component {

  render() {
    return (
      <span className="moneyTracker">
        {this.props.data.savingTotal.toFixed(2)}
      </span>
    );
  }
}


var comp = ReactDOM.render(
  <MoneyTracker data={gameStore.data}/>,
  document.getElementById('saving')
);







