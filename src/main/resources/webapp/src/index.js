import './index.scss';
import 'animate.css';

import './datastore.jsx';
console.log('datastore imported');
import * as game from './gamelogic.js';
window.game = game;
console.log('gamelogic imported');

import './networthComponent.jsx';
console.log('networth module imported');
import './assetsComponent.jsx';
import './debtComponent.jsx';
import './expensesComponent.jsx';
import './incomeComponent.jsx';
import './happynessComponent.jsx';
import './components.jsx';
import './gamificationBling.jsx';
import './chartComponent.jsx';

import 'bootstrap/dist/css/bootstrap.css';
//import 'bootstrap/dist/css/bootstrap.min.css';

console.log('Done');
