import * as mobx from 'mobx';




const {observable} = mobx;

class ObservableGameStore  {
    @observable data = {
        savings: [
            {
                principle: 0,
                savePerMonth: 0,
                type: "lonnskonto"
            },
            {
                principle: 0,
                savePerMonth: 500,
                type: "sparekonto"
            },
            {
                principle: 1000,
                savePerMonth: 0,
                type: "indexfond"
            }
        ],
        savingtypes: {
            "sparekonto": {
                name: "Sparekonto",
                interest: 1,
                risklevel: 1
            },
            "lonnskonto": {
                name: "Lønnskonto",
                interest: 1,
                risklevel: 1
            },
            "indexfond": {
                name: "Indexfond",
                interest: 7,
                risklevel: 3
            },
            aksjer: {
                name: "Aksjer",
                interest: 9,
                risklevel: 4
            }
        },
        loans: [{
            itemName: "Bil",
            principle: 90000,
            interest: 8,
            payment: 3000,
            current: 90000
        },
        {
            itemName: "Hytte",
            principle: 2500000,
            interest: 3,
            payment: 3000,
            current: 250000
        },
        {
            itemName: "Hus",
            principle: 3500000,
            interest: 3,
            payment: 3000,
            current: 350000
        }
        ],
        work: {
            monthlyPay: 30000
        },
        monthlyBills: [
        {
            itemName: "Barnehage",
            cost: 2560
        },
        {
            itemName: "Huseleie",
            cost: 3200
        },
        {
            itemName: "Mat",
            cost: 8000
        },
        {
            itemName: "Fritid/Uteliv",
            cost: 5000
        },
        {
            itemName: "Klær",
            cost: 2000
        }
        ],
        turn: 1,
        playerName: "Ole Normann",
        income: 0,
        networth: 0,
        debt: 0,
        expenses: 0,
        assets: 0,
        happymeter:-5,
        savingTotal: 0,
        chartData: {
          labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
          datasets: [{
              label: "Gjeld",
              // notice the gap in the data and the spanGaps: true
              data: [1100000, 1050000, 1000000, 950000, 880000, 830000, 790000, 750000 ,730000,710000,700000,683100],
            }, {
              label: "Sparing",
              // notice the gap in the data and the spanGaps: false
              data: [100000, 200000, 600000, 950000, 640000, 780000, 900000,1000000,700000,400000,700000,890000],
            }
          ]
        }
    };


}

window.gameStore = new ObservableGameStore();

console.log("Game store loaded...");


class GameEvents  {
    data = {
        badtsuffs: [
        {
            title: "Nabotrøbbel",
            description: "Naboen saksøker deg fordi du sagde ned et tre som han eide. Naboen vant saken!",
            moneydelta: -50000
        },
        {
            title: "Ny vaskemaskin",
            description: "Den gamle vaskemaskinen røk på spektakulært vis. Elkjip hadde bare dyre varianter inne så du endte opp med Luksusutgaven.",
            moneydelta: -8000
        },
        {
            title: "Hestetrøbbel",
            description: "Du ble med noen kompiser på veddeløpsbanen og etter et par øl ble du litt ivrig og satset for mye på hester. Du tapte alt.",
            moneydelta: -10000
        },

/*
        {
            title: "",
            description: "",
            moneydelta: 0
        }
*/
        ]
    }
}

window.events = new GameEvents();

console.log("Game events loaded...");




class ModalData  {
    data = {
        title: "Nabotrøbbel",
        description: "Naboen saksøker deg fordi du sagde ned et tre som han eide. Naboen vant saken!",
        moneydelta: -50000
    }
}

window.modalData = new ModalData();


