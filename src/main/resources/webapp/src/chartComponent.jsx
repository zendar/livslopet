 
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import * as mobx from 'mobx';
import * as mobxReact from 'mobx-react';
import { Line } from 'react-chartjs-2';

const {observable, computed,autorun} = mobx;
const {observer} = mobxReact;

@observer
class LifeChart extends Component {
  constructor(props) {
    super(props);
    this.chartReference = React.createRef();
    this._isMounted = false;

    autorun(() => {
      /* do some stuff */
      console.log("autorun on data.."+this.props.data.chartData.datasets[0].data);

      if (this._isMounted != false) {

          this.chartReference.current.chartInstance.update();
      }

    });
  }

  componentDidMount() {
    console.log("CharRef: "+this.chartReference); // returns a Chart.js instance reference
    this._isMounted = true;
//    this.props.data.poll();
  }


  render() {


    var data = {

      labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
      datasets: [{
          label: "Gjeld",
          fill: false,
          lineTension: 0.1,
          backgroundColor: "rgba(225,0,0,0.4)",
          borderColor: "red", // The main line color
          borderCapStyle: 'square',
          borderDash: [], // try [5, 15] for instance
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: "black",
          pointBackgroundColor: "white",
          pointBorderWidth: 1,
          pointHoverRadius: 8,
          pointHoverBackgroundColor: "yellow",
          pointHoverBorderColor: "brown",
          pointHoverBorderWidth: 2,
          pointRadius: 4,
          pointHitRadius: 10,
          // notice the gap in the data and the spanGaps: true
          data: this.props.data.chartData.datasets[0].data,
//          data: [1100000, 1050000, 1000000, 950000, 880000, 830000, 790000, 750000 ,730000,710000,700000,683100],
          spanGaps: true,
        }, {
          label: "Sparing",
          fill: true,
          lineTension: 0.1,
          backgroundColor: "rgba(167,105,0,0.4)",
          borderColor: "rgb(167, 105, 0)",
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: "white",
          pointBackgroundColor: "black",
          pointBorderWidth: 1,
          pointHoverRadius: 8,
          pointHoverBackgroundColor: "brown",
          pointHoverBorderColor: "yellow",
          pointHoverBorderWidth: 2,
          pointRadius: 4,
          pointHitRadius: 10,
          // notice the gap in the data and the spanGaps: false
          data: this.props.data.chartData.datasets[1].data,
    //      data: [100000, 200000, 600000, 950000, 640000, 780000, 900000,1000000,700000,400000,700000,890000],
          spanGaps: false
        }
      ]
    };


    // Notice the scaleLabel at the same level as Ticks
    var options = {
      scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    },
                    scaleLabel: {
                         display: true,
                         labelString: 'Livsløpet',
                         fontSize: 20
                      }
                }]
            },
      responsive: true,
      maintainAspectRatio: false
    };
	let something = this.props.data.chartData.datasets[0].data[0];
	console.log("chart prop data label[0]: "+something);


  	if (this._isMounted == false) {
  		console.log("NOT MOUNTED");
  	} else {
  	  console.log("We are MOUNTEDxxx");
  	}
      return (
          <div>
              <span>{something}</span>
          <Line ref={this.chartReference} data={data} options={options} />
          </div>
      );

  }
}


ReactDOM.render(
  <LifeChart data={gameStore.data}/>,
  document.getElementById('chart')
);







