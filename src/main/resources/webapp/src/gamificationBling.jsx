import $ from 'jquery';
import 'bootstrap-notify';
import 'howler';
//import 'react-bootstrap';
import React, { Component }  from 'react';
import * as ReactDOM from 'react-dom';
import { Button } from 'react-bootstrap';
import { Modal } from 'react-bootstrap';


window.showWonderMessage = function(message) {
    var sound = new Howl({
  src: ['sounds/gridbox-completed.wav']
}).play();
$.notify({
    // options
    icon: 'glyphicon glyphicon-ok',
    message: message ? message : 'default message'
},{
    // settings
    type: 'success',
    animate: {
        enter: 'animated tada',
        exit: 'animated fadeOutUp'
    }
});

}



window.showBadEventMessage = function(title, message) {
    var sound = new Howl({
  src: ['sounds/gridbox-completed.wav']
}).play();
$.notify({
    // options
    icon: 'glyphicon glyphicon-ok',
    title: title,
    message: message ? message : 'default message'
},{
    // settings
    type: 'warning',
    animate: {
        enter: 'animated tada',
        exit: 'animated fadeOutUp'
    }
});

}
  var modalBaseStyle = {
    minHzeight: "300px",
  };

  var modalStyle = {
    "marginTop": "5px",
    xbackgroundImage: "url(/images/bkg.jpg)",
    xbackground: 'black'
  };

  var modalBodyStyle = {
    "marginTop": "5px",
    xbackgroundImage: "url(/images/bkg.jpg)",
    bxackgroundRepeat: "no-repeat",
    bxackgroundSize: "cover",
    height: "100px"

  };


class Example extends Component {

  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
      title: "",
      description: "",
      money: ""
    };

    this.close = this.close.bind(this);
    this.open = this.open.bind(this);

  }


  close() {
     this.setState({ showModal:false});
  }

  open(title, description,money) {
    this.setState({ showModal: true, title: title, description: description, money: money });
  }

  render() {

    return (
      <div>
        <Modal style={modalBaseStyle} show={this.state.showModal} onHide={this.close} backdrop={'static'} keyboard={false}>
          <Modal.Header closeButton style={modalStyle}>
            <Modal.Title>{this.state.title}</Modal.Title>
          </Modal.Header>
          <Modal.Body style={modalBodyStyle}>
          {this.state.description}

          <p><br/>Utgifter: {-this.state.money}</p>
          </Modal.Body>
          <Modal.Footer style={modalStyle}>
            <Button onClick={this.close} variant="primary">Lukk</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

var modalo = ReactDOM.render(<Example data={modalData.data} />, mountNode);


window.showModal = function(title, descrption, money) {
  modalo.open(title, descrption, money);
}

