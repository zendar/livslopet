
import React from 'react';
import ReactDOM from 'react-dom';

import * as mobx from 'mobx';
import * as mobxReact from 'mobx-react';

const {observable, computed,autorun} = mobx;
const {observer} = mobxReact;

@observer
class NetworthTracker extends React.Component {

  render() {
    return (
      <span className="moneyTracker">
        {this.props.data.networth.toFixed(2)}
      </span>
    );
  }
}


ReactDOM.render(
  <NetworthTracker data={gameStore.data}/>,
  document.getElementById('networth')
);







