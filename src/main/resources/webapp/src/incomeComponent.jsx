
import React from 'react';
import ReactDOM from 'react-dom';

import * as mobx from 'mobx';
import * as mobxReact from 'mobx-react';

const {observable, computed,autorun} = mobx;
const {observer} = mobxReact;

@observer
class IncomeTracker extends React.Component {

  render() {
    return (
      <span className="incomeTracker">
        {this.props.data.work.monthlyPay.toFixed(2)}
      </span>
    );
  }
}


ReactDOM.render(
  <IncomeTracker data={gameStore.data}/>,
  document.getElementById('income')
);

