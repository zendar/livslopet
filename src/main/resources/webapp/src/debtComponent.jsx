
import React from 'react';
import ReactDOM from 'react-dom';

import * as mobx from 'mobx';
import * as mobxReact from 'mobx-react';

const {observable, computed,autorun} = mobx;
const {observer} = mobxReact;

@observer
class DebtTracker extends React.Component {

  render() {
    return (
      <span className="debtTracker">
        {this.props.data.debt.toFixed(2)}
      </span>
    );
  }
}


ReactDOM.render(
  <DebtTracker data={gameStore.data}/>,
  document.getElementById('debt')
);







