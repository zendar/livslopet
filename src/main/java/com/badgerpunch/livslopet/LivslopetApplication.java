package com.badgerpunch.livslopet;

import com.badgerpunch.livslopet.health.TemplateHealthCheck;
import com.badgerpunch.livslopet.models.Person;
import com.badgerpunch.livslopet.models.SavingsAccount;
import com.badgerpunch.livslopet.resources.GameResource;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class LivslopetApplication extends Application<LivslopetConfiguration> {

    private static final int MAX_TICK_COUNT = 12;

    private static Person person;



    public static void main(String[] args) throws Exception {
        new LivslopetApplication().run(args);
    }

    @Override
    public String getName() {
        return "hello-world";
    }

    @Override
    public void initialize(Bootstrap<LivslopetConfiguration> bootstrap) {

        bootstrap.addBundle(new AssetsBundle("/webapp", "/", "index.html"));
    }

    @Override
    public void run(LivslopetConfiguration configuration,
                    Environment environment) {
        // nothing to do yet

        final GameResource resource = new GameResource(
                configuration.getTemplate()
        );

        final TemplateHealthCheck healthCheck =
                new TemplateHealthCheck(configuration.getTemplate());
        environment.healthChecks().register("template", healthCheck);



        environment.jersey().register(resource);



        // Start the fucker
        LivslopetApplication game = new LivslopetApplication();

    }


    public LivslopetApplication()
    {

//        testRun();
    }

    private void testRun() {
        testPerson();

        int count = 1;

        while (count <= MAX_TICK_COUNT) {

            System.out.println("TICK - "+count +" / "+MAX_TICK_COUNT);

            doTick();

            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            count++;
        }
    }


    private void doTick() {



        SavingsAccount account = person.accounts.get(0);

        account.value = account.value + account.value * account.interestRate / 12.0 / 100.0;



        System.out.printf("Savings = %.2f \n",account.value);

    }


    private void testPerson() {


        person = new Person();

        SavingsAccount account = new SavingsAccount();
        account.value = 10000;
        account.interestRate = 7;

        person.accounts.add(account);


    }




}
