package com.badgerpunch.livslopet;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by ricki on 01/08/16.
 */
public class LivslopetConfiguration extends Configuration {

    @NotEmpty
    private String template;


    @JsonProperty
    public String getTemplate() {
        return template;
    }

    @JsonProperty
    public void setTemplate(String template) {
        this.template = template;
    }

}
