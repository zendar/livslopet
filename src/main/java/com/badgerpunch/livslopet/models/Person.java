package com.badgerpunch.livslopet.models;

import java.util.ArrayList;

/**
 * Created by ricki on 29/07/16.
 */
public class Person {

    private static final int START_AGE = 20;

    public int age = START_AGE;

    public ArrayList<SavingsAccount> accounts = new ArrayList<>();
    public ArrayList<Loan> loans = new ArrayList<>();

}
