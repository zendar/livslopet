package com.badgerpunch.livslopet.models;

/**
 * Created by ricki on 29/07/16.
 */
public class Loan {

    public final double originalPrinciple;
    public double currentPrinciple;
    public double APR;


    public Loan(double originalPrinciple) {
        this.originalPrinciple = originalPrinciple;
    }
}
