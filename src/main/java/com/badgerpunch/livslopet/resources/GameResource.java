package com.badgerpunch.livslopet.resources;

import com.codahale.metrics.annotation.Timed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by ricki on 01/08/16.
 */
@Path("/game/df")
@Produces(MediaType.TEXT_HTML)
public class GameResource {

    private final String template;
    private final AtomicLong counter;


    public GameResource(String template) {
        this.template = template;
        this.counter = new AtomicLong();
    }



    @GET
    @Timed
    public String sayHello(@QueryParam("name") Optional<String> name) {
        final String value = String.format(template, name.orElse("defaultname"));
        counter.incrementAndGet();

        return value;
    }

}
