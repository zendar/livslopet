# Livsløpet
 
## Description:

Livsløpet lets you play an imaginary persons life, with different events occuring. You get to setup insurances, saving plans and handle events as they occur. Try not to go bankrupt before you become a pensioner.


### Frameworks

React, Webpack, Dropwizrd, Maven, Bootstrap

Compile:

* npx webpack to compile react javascript stuff into bundles
* mvn package *to compile the Java server-side component*

Usage:

* In _'target'_ directory:
 * java -jar Livslopet-1.0-SNAPSHOT.jar server classes/config.yml






